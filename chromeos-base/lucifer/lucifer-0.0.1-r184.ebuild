# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2.

EAPI=7

CROS_WORKON_COMMIT="0ad0147a1032b30151ab653438e3962ae2dee2d2"
CROS_WORKON_TREE="f380dc71b026f7030d8da57fd21362ba78ccfae9"
CROS_WORKON_PROJECT="chromiumos/infra/lucifer"
CROS_WORKON_LOCALNAME="../../infra/lucifer"

CROS_GO_BINARIES=(
	"lucifer/cmd/lucifer"
)

inherit cros-workon cros-go

DESCRIPTION="Chromium OS testing infrastructure"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/infra/lucifer/"

LICENSE="BSD-Google"
KEYWORDS="*"
IUSE=""
RESTRICT="binchecks strip"

DEPEND="
	dev-go/errors:=
	dev-go/gcp-bigquery:=
	dev-go/go-sys:=
	dev-go/luci-tsmon:=
	dev-go/opencensus:=
	dev-go/subcommands:=
	dev-go/xerrors:=
"
RDEPEND=""
